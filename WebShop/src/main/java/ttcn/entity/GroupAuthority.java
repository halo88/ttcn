package ttcn.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "group_authorities")
public class GroupAuthority implements Serializable{
	@Id
	private String authority_role;
	@OneToMany(mappedBy = "groupAuthor", fetch = FetchType.EAGER)
	private List<Authority> listAuthor;

	public GroupAuthority() {
	}

	public String getAuthority_role() {
		return authority_role;
	}

	public void setAuthority_role(String authority_role) {
		this.authority_role = authority_role;
	}

	public List<Authority> getListAuthor() {
		return listAuthor;
	}

	public void setListAuthor(List<Authority> listAuthor) {
		this.listAuthor = listAuthor;
	}

}
