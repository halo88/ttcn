package ttcn.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User implements Serializable{

	@Id
	private String username;
	private String password;
	private String enabled;
	private String confirmcode;
	// quan he 1-1
	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private Authority user;
	@OneToMany(mappedBy = "username", fetch = FetchType.EAGER)
	private List<Order> listOrders;
	@OneToOne(fetch=FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private UserDetail userDetail;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getConfirmcode() {
		return confirmcode;
	}

	public void setConfirmcode(String confirmcode) {
		this.confirmcode = confirmcode;
	}

	public Authority getUser() {
		return user;
	}

	public void setUser(Authority user) {
		this.user = user;
	}

}
