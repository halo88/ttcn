package ttcn.repository;

import org.springframework.data.repository.CrudRepository;

import ttcn.entity.Authority;

public interface AuthorityRepository extends CrudRepository<Authority, String>{

}
