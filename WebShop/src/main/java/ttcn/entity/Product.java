package ttcn.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Product implements Serializable{
	@Id
	private int id;
	private String productName;
	private String addDate;
	private int price;
	private int stock;
	private int categoryId;
	private int promotionId;
	
	@ManyToOne(fetch =FetchType.EAGER)
	@JoinColumn(name="promoId")
	private Promotion promotion;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="categoryId")
	private Category category;
	
	@OneToOne(fetch=FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private ProductDescription proDescription;
	
	@OneToMany(mappedBy="product", fetch=FetchType.EAGER)
	private List<ProductImage> listProImages;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public ProductDescription getProDescription() {
		return proDescription;
	}

	public void setProDescription(ProductDescription proDescription) {
		this.proDescription = proDescription;
	}

	public List<ProductImage> getListProImages() {
		return listProImages;
	}

	public void setListProImages(List<ProductImage> listProImages) {
		this.listProImages = listProImages;
	}

	public Product() {
		super();
	}
	
	
	
}
