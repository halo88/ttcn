package ttcn.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "authorities")
public class Authority implements Serializable{
	@Id
	private String username;
	private String authority;
	@OneToOne(fetch=FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private User user;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "authoriry")
	private GroupAuthority groupAuthor;	

	public Authority() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public GroupAuthority getGroupAuthor() {
		return groupAuthor;
	}

	public void setGroupAuthor(GroupAuthority groupAuthor) {
		this.groupAuthor = groupAuthor;
	}
	

}
