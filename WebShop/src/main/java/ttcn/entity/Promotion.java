package ttcn.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Promotion implements Serializable{
	@Id
	private int promoID;
	private String promoName;
	private int promoDesc;
	private String startdate;
	private String enddate;
	
	@OneToMany(mappedBy="promotion", fetch=FetchType.EAGER)
	private List<Product> listProducts;

	public int getPromoID() {
		return promoID;
	}

	public void setPromoID(int promoID) {
		this.promoID = promoID;
	}

	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public int getPromoDesc() {
		return promoDesc;
	}

	public void setPromoDesc(int promoDesc) {
		this.promoDesc = promoDesc;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public List<Product> getListProducts() {
		return listProducts;
	}

	public void setListProducts(List<Product> listProducts) {
		this.listProducts = listProducts;
	}

	public Promotion() {
		super();
	}
	
}
