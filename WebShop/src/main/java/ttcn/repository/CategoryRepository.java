package ttcn.repository;

import org.springframework.data.repository.CrudRepository;

import ttcn.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer>{

}
